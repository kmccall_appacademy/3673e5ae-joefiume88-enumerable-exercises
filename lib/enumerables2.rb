require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  return 0 if arr.empty?
  arr.reduce(:+)
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.all? do |string|
    string.include?(substring)
  end
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  counter_hash = Hash.new(0)

  string.each_char do |char|
    next if char == ' '
    counter_hash[char] += 1
  end

  counter_hash.select {|letters, values| values > 1}.keys
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  string_hash = Hash.new(0)

  string.split.each do |word|
    string_hash[word] = word.length
  end

  arr = string_hash.sort_by {|words, lengths| lengths}
  [arr.last.first, arr[-2].first]
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  alphabet = ('a'..'z').to_a

  string.each_char do |letter|
    alphabet.delete(letter)
  end

  alphabet
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  (first_yr..last_yr).to_a.select do |year|
    not_repeat_year?(year)
  end
end

def not_repeat_year?(year)
  year.to_s.chars.uniq == year.to_s.chars
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  songs_iterate = songs.dup

  songs_iterate.each_with_index do |song, idx|
    next if idx == 0
    if songs_iterate[idx - 1] == songs_iterate[idx]
      songs.delete(song)
    end
  end

  songs.uniq
end

def no_repeats?(song_name, songs)
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  distance = []
  str_arr = string.split

  str_arr.each do |word|
    distance << c_distance(word)
  end

  remove_punctuation(str_arr[distance.index(distance.max)])
end

def c_distance(word)
  res = []
  word = remove_punctuation(word)

  word.each_char.with_index do |char, idx|
     if char == 'c'
       res << idx
     else
       res << -1
     end
  end

  res.max - word.length
end

def remove_punctuation(word)
  word.each_char do |letter|
    word.delete!(letter) unless ('a'..'z').include?(letter)
  end
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  res = []
  current_idx = 0

  arr.each_index do |idx|
    next if idx == 0
    next if arr[idx] == arr[idx - 1]

    if current_idx < idx - 1
      res << [current_idx, idx - 1]
    end

    current_idx = idx
  end

  if current_idx < arr.length - 1
    res << [current_idx, arr.length - 1]
  end

  res
end
